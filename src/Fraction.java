import java.util.*;

/** This class represents fractions of form n/d where n and d are integer 
 * numbers. Basic operations and arithmetics for fractions are provided.
 */
public class Fraction implements Comparable<Fraction> {

   /** Main method. Different tests. */
   public static void main (String[] param) {
      System.out.println((int)Math.round(-10.*5));
      System.out.println(Fraction.valueOf("3/5"));
      System.out.println(Fraction.valueOf("mis see on"));
   }

   private int _denom, _num;

   /** Constructor.
    * @param a numerator
    * @param b denominator > 0
    */

   public Fraction () {this(1,1);}

   public Fraction(int a) {this (a,1);}

   public Fraction (int a, int b) {
      if (b<=0) throw new ArithmeticException("Lugeja peab olema suurem, kui 0.");
      else {
         _num = a;
         _denom = b;
      }
   }

   /** Public method to access the numerator field. 
    * @return numerator
    */
   public int getNumerator() {
      return _num;
   }

   /** Public method to access the denominator field. 
    * @return denominator
    */
   public int getDenominator() {
      return _denom;
   }

   /** Conversion to string.
    * @return string representation of the fraction
    */
   @Override
   public String toString() {
      return _num+"/"+_denom;
   }

   /** Equality test.
    * @param m second fraction
    * @return true if fractions this and m are equal
    */
   @Override
   public boolean equals (Object m) {
      if (m != null) {
         ((Fraction) m).reduce();
         reduce();
         if (((Fraction) m)._denom != _denom || ((Fraction) m)._num != _num) return false;
      }
      return true;
   }

   /** Hashcode has to be equal for equal fractions.
    * @return hashcode
    */
   @Override
   public int hashCode() {
      return toString().hashCode();
      /* Liiga lihtne lahendus - üleval parem
      int hash = 13;
      hash = 17*hash + _num;
      hash = 31*hash + _denom;
      return hash;
      */
   }

   /** Sum of fractions.
    * @param m second addend
    * @return this+m
    */
   public Fraction plus (Fraction m) {
      Fraction output = new Fraction();
      output._denom = ((Fraction)m)._denom*_denom;
      output._num = _num*((Fraction)m)._denom + ((Fraction)m)._num * _denom;
      output.reduce();
      return output;
   }

   /** Multiplication of fractions.
    * @param m second factor
    * @return this*m
    */
   public Fraction times (Fraction m) {
      Fraction output = new Fraction();
      output._num = m._num * _num;
      output._denom = m._denom * _denom;
      output.reduce();
      return output;
   }

   /** Inverse of the fraction. n/d becomes d/n.
    * @return inverse of this fraction: 1/this
    */
   public Fraction inverse() throws ArithmeticException {
      if (_num<0) {
         _denom = -_denom;
         _num = -_num;
      }
      return new Fraction(_denom, _num);
   }

   /** Opposite of the fraction. n/d becomes -n/d.
    * @return opposite of this fraction: -this
    */
   public Fraction opposite() {
      return new Fraction(-_num, _denom);
   }

   /** Difference of fractions.
    * @param m subtrahend
    * @return this-m
    */
   public Fraction minus (Fraction m) {
      return new Fraction(_num, _denom).plus(m.opposite());
   }

   /** Quotient of fractions.
    * @param m divisor
    * @return this/m
    */
   public Fraction divideBy (Fraction m) throws ArithmeticException {
      return times(m.inverse());
   }

   /** Comparision of fractions.
    * @param m second fraction
    * @return -1 if this < m; 0 if this==m; 1 if this > m
    */
   @Override
   public int compareTo (Fraction m) {
      if (_num * m._denom <= _denom * m._num) {
         if (_num * m._denom == _denom * m._num) return 0;
         return -1;
      }
      return 1;
   }

   /** Clone of the fraction.
    * @return new fraction equal to this
    */
   @Override
   public Object clone() throws CloneNotSupportedException {
      return new Fraction(_num, _denom);
   }

   /** Integer part of the (improper) fraction. 
    * @return integer part of this fraction
    */
   public int integerPart() {
      return _num/_denom;
   }

   /** Extract fraction part of the (improper) fraction
    * (a proper fraction without the integer part).
    * @return fraction part of this fraction
    */
   public Fraction fractionPart() {
      return this.minus(new Fraction(this.integerPart()));
   }

   /** Approximate value of the fraction.
    * @return numeric value of this fraction
    */
   public double toDouble() {
      return (double)_num/(double)_denom;
   }

   /** Double value f presented as a fraction with denominator d > 0.
    * @param f real number
    * @param d positive denominator for the result
    * @return f as an approximate fraction of form n/d
    */
   public static Fraction toFraction (double f, int d) {
      return new Fraction((int)Math.round(f*d) ,d);
   }

   /** Conversion from string to the fraction. Accepts strings of form
    * that is defined by the toString method.
    * @param s string form (as produced by toString) of the fraction
    * @return fraction represented by s
    */
   public static Fraction valueOf (String s) {
      StringTokenizer string = new StringTokenizer(s, "/");
      Fraction output = new Fraction();
      try {
         output._num = Integer.parseInt(string.nextElement().toString());
         output._denom = Integer.parseInt(string.nextElement().toString());
      } catch (NumberFormatException e) {
         System.out.println("Viga sisendis: "+s);
         return null;
      }
      return output;
   }

   private int gcd() {
      return gcdRec(_num, _denom);
   }

   private int gcdRec(int a, int b) {
      if (b == 0) return a;
      return gcdRec(b, a%b);
   }

   private void reduce() {
      int divider = gcd();
      _num /= divider;
      _denom /= divider;
      simplify();
   }

   private void simplify() {
      if (_denom < 0) {
         _num = -_num;
         _denom = -_denom;
      }
   }
}